from django.conf import settings
from django.http import HttpResponse, JsonResponse
from wsgiref.util import FileWrapper

from somoffice.core.http import requires_authentication, with_secure_token

from somoffice.core import load_resource_provider


@requires_authentication
def index(request):
    options = {
        "user": request.user,
        "remote_user_id": request.user.profile.remoteId,
    }

    provider = load_resource_provider("invoices")(options)

    return JsonResponse(provider.get_resources(), safe=False,)


@requires_authentication
@with_secure_token(secured_param="secure_id")
def check(request, secure_id, secure_token):
    options = {
        "remote_user_id": request.user.profile.remoteId,
    }

    provider = load_resource_provider("invoices")(options)

    if provider.is_invoice_ready_to_download(secure_token["resource_id"]):
        return HttpResponse(status=200)
    else:
        return HttpResponse(status=404)


@requires_authentication
@with_secure_token(secured_param="secure_id")
def download(request, secure_id, filename, secure_token):
    options = {
        "remote_user_id": request.user.profile.remoteId,
    }

    provider = load_resource_provider("invoices")(options)

    invoice_tmp_path = provider.download_invoice(secure_token["resource_id"])

    if not invoice_tmp_path:
        return HttpResponse(status=404)

    invoice_file = open(invoice_tmp_path, "rb")
    wrapper = FileWrapper(invoice_file)

    response = HttpResponse(wrapper, content_type="application/pdf")
    response["Content-Disposition"] = 'attachment; filename="%s"' % filename

    return response
