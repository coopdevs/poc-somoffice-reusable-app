from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from somoffice.core.http import with_secure_token
from somoffice.core import load_resource_provider


class SubscriptionsViewSet(ViewSet):
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_value_regex = "[^/]+"

    def list(self, request):
        options = {
            "remote_user_id": request.user.profile.remoteId,
            "language": request.user.profile.preferredLocale,
        }

        provider = load_resource_provider("subscriptions")(options)

        return Response(provider.get_resources())

    @with_secure_token(uses_django_rest=True)
    def retrieve(self, request, pk, secure_token):
        options = {
            "remote_user_id": request.user.profile.remoteId,
            "language": request.user.profile.preferredLocale,
        }

        provider = load_resource_provider("subscriptions")(options)

        return Response(
            provider.get_resource(secure_token["resource_id"])
        )
