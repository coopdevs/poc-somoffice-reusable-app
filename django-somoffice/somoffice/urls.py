"""somoffice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from somoffice.views import auth
from somoffice.views import invoices
from somoffice.views import users
from somoffice.views.available_providers import AvailableProviders
from somoffice.views.discovery_channels import DiscoveryChannels
from somoffice.views.products import Products
from somoffice.views.product_catalog import ProductCatalog
from somoffice.views.tickets import TicketsViewSet
from somoffice.views.subscriptions import SubscriptionsViewSet
from somoffice.views.services_form import ServicesForm
from somoffice.views.profile import ProfileViewSet
from rest_framework.routers import SimpleRouter
import django_rest_passwordreset

router = SimpleRouter()
router.register(r"subscriptions", SubscriptionsViewSet, basename="subscriptions")
router.register(r"profile", ProfileViewSet, basename="profile")
router.register(r"tickets", TicketsViewSet, basename="tickets")

urlpatterns = [
    path("admin/", admin.site.urls),
    # auth
    path("api/auth/login/", auth.login_view),
    path("api/auth/logout/", auth.logout_view),
    path("api/auth/set-csrf-cookie/", auth.set_csrf_cookie),
    # TODO #django-rest-passwordreset, just include django_rest_passwordreset urls
    # path("api/auth/password_reset/", include("django_rest_passwordreset.urls")),
    path("api/auth/password_reset/", auth.reset_password_request_token_from_username),
    path(
        "api/auth/password_reset/validate_token/",
        django_rest_passwordreset.views.reset_password_validate_token,
    ),
    path(
        "api/auth/password_reset/confirm/",
        django_rest_passwordreset.views.reset_password_confirm,
    ),
    # TODO: Se necesitan las APIs de Subscription??
    # endpoints
    # path("api/subscriptions/", Subscriptions.as_view()),
    # path("api/subscriptions/<str:secure_id>", subscriptions.show),
    path("api/discovery-channels/", DiscoveryChannels.as_view()),
    path("api/services-form/submit/", ServicesForm.as_view()),
    path("api/invoices/", invoices.index),
    path("api/invoices/check/<str:secure_id>", invoices.check),
    path("api/invoices/download/<str:secure_id>/<str:filename>", invoices.download),
    path("api/user/exists/", users.search_by_vat, name='user_exists'),
    path("api/user/check/", users.check_user, name='check_user'),
    path("api/products/", Products.as_view()),
    path("api/providers/", AvailableProviders.as_view()),
    path("api/product-catalog/", ProductCatalog.as_view()),
    path("api/", include(router.urls)),
]
