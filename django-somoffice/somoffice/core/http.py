import jwt
from django.http import JsonResponse
from django.conf import settings
from django.utils.translation.trans_real import parse_accept_lang_header
from django.utils import translation
from somoffice.core.secure_resources import validate_secure_id


def requires_authentication(fn):
    def func_wrapper(*args, **kwargs):
        request = args[0]
        if request.user.is_authenticated:
            return fn(*args, **kwargs)
        else:
            return JsonResponse({"reason": "authentication_required"}, status=403)

    return func_wrapper


class with_secure_token:
    def __init__(self, secured_param="pk", uses_django_rest=False):
        self.secured_param = secured_param
        self.uses_django_rest = uses_django_rest

    def __call__(self, fn):
        def func_wrapper(*args, **kwargs):
            request = self._get_request(*args, **kwargs)
            secure_id = self._get_secure_id(*args, **kwargs)

            secure_token = validate_secure_id(request.user, secure_id)

            # TODO #doc document secured_token authorization mechanism
            if secure_token is None:
                return JsonResponse({"reason": "access denied"}, status=403)
            else:
                self._inject_decrypted_param(secure_token, kwargs)
                return fn(*args, **kwargs)

        return func_wrapper

    def _get_request(self, *args, **kwargs):
        if self.uses_django_rest:
            return args[1]
        else:
            return args[0]

    def _get_secure_id(self, *args, **kwargs):
        request = self._get_request(*args, **kwargs)

        return kwargs.get(self.secured_param) or request.query_params.get(
            self.secured_param
        )

    def _inject_decrypted_param(self, secure_token, kwargs):
        kwargs["secure_token"] = secure_token


def locale_middleware(get_response):
    DEFAULT_LANGUAGE = "ca"

    # One-time configuration and initialization.

    def locale_from_headers(request):
        supported_languages = [lang[0] for lang in settings.LANGUAGES]
        accept_lang_locales = parse_accept_lang_header(
            request.headers.get("Accept-Language", "")
        )

        for [locale, q] in accept_lang_locales:
            language = locale.split("-")[0]
            if language in supported_languages:
                return language

    def middleware(request):
        if request.user and not request.user.is_anonymous:
            locale = request.user.profile.preferredLocale
        else:
            locale = locale_from_headers(request)

        locale = locale or DEFAULT_LANGUAGE

        # Code to be executed for each request before
        # the view (and later middleware) are called.

        translation.activate(locale)

        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware
