import jwt
from django.http import JsonResponse
from django.conf import settings


def build_secure_id_generator(user_id, resource_type):
    def secure_id_generator(resource_id):
        return jwt.encode(
            {
                "user_id": user_id,
                "resource_type": resource_type,
                "resource_id": resource_id,
            },
            settings.SECRET_KEY,
        ).decode("utf-8")

    return secure_id_generator


def validate_secure_id(user, secure_id):
    try:
        secure_token = jwt.decode(secure_id, settings.SECRET_KEY, algorithms=["HS256"])

        if secure_token["user_id"] != user.profile.remoteId:
            return None
        else:
            return secure_token
    except jwt.exceptions.InvalidSignatureError:
        return None
    except jwt.exceptions.DecodeError:
        return None
