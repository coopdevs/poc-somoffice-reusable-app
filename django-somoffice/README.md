## Django SomOffice Core App

### URLs
### Providers
In your SomOffice implementation you need to define the next providers:

* invoices
* subscriptions
* services_form
* users
* products
* product_catalog
* tickets
* available_providers
* discovery_channels
* profile

This providers have the next API required:

```python

class ExampleProvider:
  def __init__(self, options):
    self.options = options

  def get_resource(self):
    ...
    return serializer.data  # json

  def get_resources(self):
    ...
    return serializer.data  # json

```

### Templates

* Password reset
* Confirm email change
