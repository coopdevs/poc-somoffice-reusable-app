import { formToApi } from "./formToApi";

const state = {
  currentIndex: 16,
  formStepDataByKey: {
    "partner/personal-data": {
      is_company: false,
      vat: "45771114x",
      name: "jorge",
      surname: "morante",
      lastname: "cabrera",
      email: "jorge@morante.eu",
      repeat_email: "jorge@morante.eu",
      birthdate: "21-12-1987",
      gender: "male",
      lang: "es",
      nationality: "ES"
    },
    "partner/additional-data": {
      discovery_channel_id: 7
    },
    "line-0/tariff": {
      type: "internet",
      code: "SE_SC_REC_BA_F_1024"
    },
    "line-0/additional-data": {
      service_address: {
        _id: "address_1",
        place: {
          place_name:
            "Avinguda De Mistral 71, 08015 Barcelona, Barcelona, Spain",
          street: "Avinguda De Mistral, 71",
          city: "Barcelona",
          zip_code: "08015",
          state: "ES-B",
          country: "es"
        },
        door: "E-3"
      },
      previous_broadband_service: 5
    },
    "line-1/tariff": {
      type: "internet",
      code: "SE_SC_REC_BA_F_1024"
    },
    "line-1/additional-data": {
      service_address: {
        _id: "address_1",
        place: {
          place_name:
            "Avinguda De Mistral 71, 08015 Barcelona, Barcelona, Spain",
          street: "Avinguda De Mistral, 71",
          city: "Barcelona",
          zip_code: "08015",
          state: "ES-B",
          country: "es"
        },
        door: "E-3"
      },
      previous_broadband_service: 5
    },
    "line-2/tariff": {
      type: "mobile",
      code: "SE_SC_REC_MOBILE_T_UNL_51200"
    },
    "line-2/additional-data": {
      has_sim_card: false,
      is_prepaid: false,
      keep_number: true,
      delivery_address: {
        _id: "address_1",
        place: {
          place_name:
            "Avinguda De Mistral 71, 08015 Barcelona, Barcelona, Spain",
          street: "Avinguda De Mistral, 71",
          city: "Barcelona",
          zip_code: "08015",
          state: "ES-B",
          country: "es"
        },
        door: "E-3"
      },
      previous_service: 5,
      phone_number: "658395418"
    },
    "line-3/tariff": {
      type: "mobile",
      code: "SE_SC_REC_MOBILE_T_150_2048"
    },
    "line-3/additional-data": {
      has_sim_card: false,
      is_prepaid: false,
      keep_number: true,
      delivery_address: {
        _id: "address_1",
        place: {
          place_name:
            "Avinguda De Mistral 71, 08015 Barcelona, Barcelona, Spain",
          street: "Avinguda De Mistral, 71",
          city: "Barcelona",
          zip_code: "08015",
          state: "ES-B",
          country: "es"
        },
        door: "E-3"
      },
      other_person: true,
      previous_owner_name: "Sara",
      previous_owner_surname: "Montesdeoca",
      previous_owner_vat: "55771114r",
      previous_owner_lastname: "Romero",
      previous_service: 49,
      phone_number: "606609454"
    },
    "line-4/tariff": {
      type: "mobile",
      code: "SE_SC_REC_MOBILE_T_UNL_51200"
    },
    "line-4/additional-data": {
      has_sim_card: false,
      is_prepaid: false,
      keep_number: false,
      delivery_address: {
        _id: "address_1",
        place: {
          place_name:
            "Avinguda De Mistral 71, 08015 Barcelona, Barcelona, Spain",
          street: "Avinguda De Mistral, 71",
          city: "Barcelona",
          zip_code: "08015",
          state: "ES-B",
          country: "es"
        },
        door: "E-3"
      },
      previous_service: 39,
      other_person: false
    },
    "line-5/tariff": {
      type: "mobile",
      code: "SE_SC_REC_MOBILE_T_UNL_51200"
    },
    "line-5/additional-data": {
      has_sim_card: false,
      is_prepaid: false,
      keep_number: false,
      delivery_address: {
        addNew: true,
        _id: "address_6",
        place: {
          place_name:
            "Avenida Parque Central, 35013 Las Palmas De Gran Canaria, Canary Islands, Spain",
          street: "Avenida Parque Central, ",
          city: "Las Palmas De Gran Canaria",
          zip_code: "35013",
          state: "ES-TF",
          country: "es"
        }
      }
    },
    "payment/member-fee": {
      address: {
        _id: "address_1"
      },
      pays_in_ten_terms: true,
      iban: "ES7601286945712351248624"
    },
    "payment/monthly-bill": {
      use_same_bank_account: true,
      agrees_to_voluntary_contribution: true
    }
  },
  steps: [
    "partner/personal-data",
    "partner/additional-data",
    "line-0/tariff",
    "line-0/additional-data",
    "line-1/tariff",
    "line-1/additional-data",
    "line-2/tariff",
    "line-2/additional-data",
    "line-3/tariff",
    "line-3/additional-data",
    "line-4/tariff",
    "line-4/additional-data",
    "line-5/tariff",
    "line-5/additional-data",
    "payment/monthly-bill",
    "payment/member-fee",
  ],
  lines: [
    {
      type: "internet"
    },
    {
      type: "internet"
    },
    {
      type: "mobile"
    },
    {
      type: "mobile"
    },
    {
      type: "mobile"
    },
    {
      type: "mobile"
    }
  ],
  availableAddresses: [
    {
      _id: "address_1",
      place: {
        place_name: "Avinguda De Mistral 71, 08015 Barcelona, Barcelona, Spain",
        street: "Avinguda De Mistral, 71",
        city: "Barcelona",
        zip_code: "08015",
        state: "ES-B",
        country: "es"
      },
      door: "E-3"
    },
    {
      _id: "address_6",
      place: {
        place_name:
          "Avenida Parque Central, 35013 Las Palmas De Gran Canaria, Canary Islands, Spain",
        street: "Avenida Parque Central, 10, 8-D",
        city: "Las Palmas De Gran Canaria",
        zip_code: "35013",
        state: "ES-TF",
        country: "es"
      }
    }
  ]
};

describe("submitToApi", () => {
  it("transforms the state to a format the API can understand", () => {
    expect(submitToApi(state)).toEqual({
      is_company: false,
      vat: "es45771114x",
      name: "jorge",
      surname: "morante",
      lastname: "cabrera",
      email: "jorge@morante.eu",
      birthdate: "21-12-1987",
      gender: "male",
      lang: "es",
      nationality: "ES",
      discovery_channel_id: 7,
      payment_type: "split",
      iban: "ES7601286945712351248624",
      service_iban: "ES7601286945712351248624",
      agrees_to_voluntary_contribution: true,
      address: {
        street: "Avinguda De Mistral, 71, E-3",
        city: "Barcelona",
        zip_code: "08015",
        state: "ES-B",
        country: "es"
      },
      lines: [
        {
        }
      ]
    });
  });
});
