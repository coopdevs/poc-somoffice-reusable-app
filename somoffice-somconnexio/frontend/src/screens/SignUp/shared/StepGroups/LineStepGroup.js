import React, { useEffect, useState } from "react";
import { Container } from "components/layouts/Container";
import { Text } from "components/Text";
import { TextField } from "components/TextField";
import { Box } from "@material-ui/core";
import { Button } from "components/Button";
import { Tiles } from "components/layouts/Tiles";
import { useTranslation } from "react-i18next";
import { Subheading } from "components/Subheading";
import { RadioToggle } from "components/RadioToggle";
import { Select } from "components/Select";
import { JsonParam, useQueryParam } from "use-query-params";
import { InternetTariffPicker } from "screens/Tariffs/InternetTariffPicker";
import { MobileTariffPicker } from "screens/Tariffs/MobileTariffPicker";
import { getDiscoveryChannels } from "lib/api/discoveryChannels";
import { getAvailableProviders } from "lib/api/availableProviders";
import { genders, nationalities } from "lib/domain/somconnexio/selections";
import {
  composeValidators,
  required,
  matchDateFormat,
  matchEmailFormat,
  matchVatFormat,
  mustMatchOther
} from "lib/form/validators";
import { Condition } from "../Condition";
import { FormStep } from "../FormStep";
import { ApiSelect } from "../ApiSelect";
import { InternetAdditionalData } from "../InternetAdditionalData";
import { MobileAdditionalData } from "../MobileAdditionalData";
import { useTariffs } from "hooks/queries/useTariffs";
import { useDerivedState, useStore } from "hooks/useStore";
import { last, compact } from "lodash";
import { capitalize } from "lib/string/capitalize";
import { FormStepGroup } from "../FormStepGroup";
import { useHistory } from "react-router-dom";
import { ConfirmationSummary } from "../ConfirmationSummary";
import { getFullName } from "lib/helpers";
import { formatFullAddress } from "../formatFullAddress";

const RemoveLineButton = ({ index }) => {
  const { t } = useTranslation();
  const removeLineAt = useStore(state => state.removeLineAt);

  return (
    <Text
      onClick={() => removeLineAt(index, { updateSteps: true })}
      size="2xs"
      bold
      color="primary.main"
      uppercase
    >
      {t("funnel.shared.remove_service")}
    </Text>
  );
};

/**
 * Maybe add IconActionButton component or something like that?
 */
const AddButton = ({ text, onClick }) => {
  return (
    <Box
      display="flex"
      alignItems="center"
      style={{ cursor: "pointer" }}
      onClick={onClick}
    >
      <svg
        width="20"
        height="20"
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle cx="10" cy="10" r="9.5" stroke="#3E3382" />
        <line
          x1="4.7998"
          y1="9.8999"
          x2="15.1998"
          y2="9.8999"
          stroke="#3E3382"
        />
        <line
          x1="4.7998"
          y1="9.8999"
          x2="15.1998"
          y2="9.8999"
          stroke="#3E3382"
        />
        <line
          x1="9.90039"
          y1="15.2009"
          x2="9.90039"
          y2="4.80093"
          stroke="#3E3382"
        />
        <line
          x1="9.90039"
          y1="15.2009"
          x2="9.90039"
          y2="4.80093"
          stroke="#3E3382"
        />
      </svg>

      <Box ml={2}>
        <Text size="xs" bold uppercase selectable={false}>
          {text}
        </Text>
      </Box>
    </Box>
  );
};

const AddLine = ({ type }) => {
  const { t } = useTranslation();
  const { canAddLineOfType } = useDerivedState();
  const addMobileLine = useStore(state => state.addMobileLine);
  const addInternetLine = useStore(state => state.addInternetLine);
  const addCallback = type === "internet" ? addInternetLine : addMobileLine;
  const canAddLine = canAddLineOfType(type);

  if (!canAddLine) {
    return null;
  }

  return (
    <AddButton
      onClick={addCallback}
      text={t(`funnel.tariffs.${type}.add_line`)}
    />
  );
};

export const LineStepGroup = ({
  index,
  stepsBefore = 2,
  line,
  isLast,
  confirmMode,
  tariffs,
  isLastOfKind,
  isUniqueOfType,
  indexOfKind
}) => {
  const { t } = useTranslation();
  const saveAddress = useStore(state => state.saveAddress);
  const { getAddressById } = useDerivedState()
  const history = useHistory();

  const saveAddressFromField = address => values => {
    saveAddress(values[address]);
  };

  const renderLineSummary = ({ code }) => {
    const tariff = tariffs.find(tariff => tariff.code === code);

    if (!tariff) {
      return "";
    }

    if (confirmMode) {
      return (
        <ConfirmationSummary
          title={t(
            "funnel.signup.data.steps.internet_line_additional_data.confirmation.title"
          )}
          i18nPrefix="funnel.signup.data.steps.internet_line_additional_data.confirmation"
          fields={{
            contracted_service: tariff.name
          }}
        />
      );
    }

    return (
      <Text size="xs">
        {tariff.name} ({tariff.price}
        {t("common.euros_per_month")})
      </Text>
    );
  };

  const renderInternetAdditionalDataSumary = values => {
    if (!confirmMode) {
      return (
        <Text size="xs">
          {values.service_address && formatFullAddress(getAddressById(values.service_address._id))}
        </Text>
      );
    }

    return (
      <ConfirmationSummary
        title={t(
          "funnel.signup.data.steps.internet_line_additional_data.confirmation.service_address"
        )}
        i18nPrefix="funnel.signup.data.steps.internet_line_additional_data.confirmation"
        fields={{
          service_address: values.service_address && formatFullAddress(getAddressById(values.service_address._id))
        }}
      />
    );
  };

  const renderMobileAdditionalDataSumary = values => {
    if (!confirmMode) {
      return (
        <Text size="xs">
          {compact([
            values.phone_number,
            values.other_person &&
              [
                values.previous_owner_name,
                values.previous_owner_surname,
                values.previous_owner_lastname
              ]
                .map(capitalize)
                .join(" "),
            values.previous_owner_vat
          ]).join(" · ")}
        </Text>
      );
    }

    return (
      <ConfirmationSummary
        title={t(
          "funnel.signup.data.steps.mobile_line_additional_data.confirmation.title"
        )}
        i18nPrefix="funnel.signup.data.steps.mobile_line_additional_data.confirmation"
        labelResolvers={{
          previous_provider: () =>
            getAvailableProviders({ category: line.type })
        }}
        fields={{
          phonenumber: values.phone_number,
          other_person_fullname: values.other_person
            ? getFullName(values, {
                name: "previous_owner_name",
                surname: "previous_owner_surname",
                lastname: "previous_owner_lastname"
              })
            : null,
          other_person_vat: values.previous_owner_vat,
          previous_provider: values.previous_provider,
          current_contract_type: null,
          delivery_address: values.delivery_address ? formatFullAddress(getAddressById(values.delivery_address._id)) : null
        }}
      />
    );
  };

  const titleI18nKey = isUniqueOfType
    ? `funnel.shared.category.${line.type}`
    : `funnel.shared.nth_${line.type}_line`;

  return (
    <>
      <FormStepGroup
        index={stepsBefore + index}
        title={t(titleI18nKey, { n: `${indexOfKind + 1}a` })}
        confirmMode={confirmMode}
        topRightAction={
          !confirmMode && indexOfKind > 0 && <RemoveLineButton index={index} />
        }
      >
        <FormStep
          id={`line-${line.__id}/tariff`}
          title="Tarifa"
          initialValues={line}
          renderSummary={renderLineSummary}
          omitTitleWhenOpen
        >
          {line.type === "internet" ? (
            <InternetTariffPicker.FormField
              name="code"
              tariffs={tariffs}
              showSubmit
            />
          ) : (
            <MobileTariffPicker.FormField
              name="code"
              tariffs={tariffs}
              showSubmit
            />
          )}
        </FormStep>
        <FormStep
          id={`line-${line.__id}/additional-data`}
          title={t('funnel.shared.additional_info')}
          renderSummary={values =>
            line.type === "internet"
              ? renderInternetAdditionalDataSumary(values)
              : renderMobileAdditionalDataSumary(values)
          }
          initialValues={
            line.type === "internet"
              ? {
                  already_has_service: true,
                  internet_without_phone: false
                }
              : {
                  has_sim_card: false,
                  is_prepaid: false,
                  keep_number: true,
                }
          }
          onSubmit={values => {
            saveAddressFromField(
              line.type === "internet" ? "service_address" : "delivery_address"
            )(values);

            if (!isLast) {
              return;
            }

            history.push("payment");
          }}
        >
          {line.type === "internet" ? (
            <InternetAdditionalData line={line} isLast={isLast} />
          ) : (
            <MobileAdditionalData line={line} isLast={isLast} />
          )}
        </FormStep>
      </FormStepGroup>
    </>
  );
};
