import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Divider,
  Hidden,
  List,
  ListItem,
  makeStyles,
  Radio,
  Typography,
} from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import { Button } from "components/Button";
import { Spinner } from "components/Spinner";
import { Modal } from "components/Modal";
import { Link } from "components/Link";
import { Tiles } from "components/layouts/Tiles";
import groupBy from "lodash.groupby";
import React, { Fragment, useEffect, useState } from "react";
import { useAsync } from "react-async-hook";
import { Trans, useTranslation } from "react-i18next";
import { noop } from "lib/fn/noop";
import { Stack } from "components/layouts/Stack";

const useStyles = makeStyles((theme) => ({
  accordionHeader: {
    backgroundColor: theme.palette.grey[200],
  },
  list: {
    width: "100%",
  },
  item: {
    cursor: "pointer",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
}));

const formatPrice = (price) => Number(price).toFixed(2);

const ProductWrapper = ({
  allProducts,
  groupProductsBy,
  renderGroupName,
  renderProductList,
}) => {
  const shouldGroup = Boolean(groupProductsBy);
  const classes = useStyles();

  if (shouldGroup) {
    const groupedProducts = groupBy(allProducts, groupProductsBy);

    return (
      <>
        {Object.keys(groupedProducts).map((groupKey) => (
          <Accordion key={groupKey}>
            <AccordionSummary classes={{ root: classes.accordionHeader }}>
              {renderGroupName(groupKey)}
            </AccordionSummary>
            <AccordionDetails>
              {renderProductList(groupedProducts[groupKey])}
            </AccordionDetails>
          </Accordion>
        ))}
      </>
    );
  } else {
    return <>{renderProductList(allProducts)}</>;
  }
};

const ProductList = ({
  products,
  selectedProductCode,
  onProductSelected,
  renderProductDescription,
  priceProductMonthly,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const labelProductPrice = Boolean(priceProductMonthly)
    ? "common.euros_per_month"
    : "common.euros";

  return (
    <List classes={{ root: classes.list }} dense disablePadding={true}>
      {products.map((product, index) => {
        const productCode = product.code;

        return (
          <Fragment key={productCode}>
            <ListItem disableGutters>
              <div
                className={classes.item}
                onClick={() => onProductSelected(product)}
              >
                <div>
                  <Radio
                    value={productCode}
                    checked={selectedProductCode === productCode}
                  />
                  <Typography variant="caption">
                    {renderProductDescription(product)}
                  </Typography>
                </div>
                <div>
                  {formatPrice(product.price)} {t(labelProductPrice)}
                </div>
              </div>
            </ListItem>
            {index < products.length - 1 && <Divider />}
          </Fragment>
        );
      })}
    </List>
  );
};

export const ProductPicker = ({
  title,
  submitText,
  selectedText,
  currentText,
  descriptionText,
  confirmText,
  onLoadingStateChange,
  subscription,
  getProducts,
  groupProductsBy,
  renderGroupName,
  renderProductDescription,
  renderConfirmationStep,
  onSubmit,
  onClickClose,
  priceProductMonthly,
}) => {
  const { t } = useTranslation();
  const products = useAsync(getProducts, [subscription.id]);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [isSelected, setIsSelected] = useState(false);
  const [needsConfirmation, setNeedsConfirmation] = useState(null);
  const [needsInformation, setNeedsInformation] = useState(true);
  const [confirmationContext, setConfirmationContext] = useState(null);
  const [hasError, setHasError] = useState(false);
  const [value, setValue] = useState(null);

  useEffect(() => {
    onLoadingStateChange(isSubmitting || products.loading);
  }, [isSubmitting, products.loading, onLoadingStateChange]);

  if (products.loading || isSubmitting) {
    return <Spinner />;
  }

  if (isSubmitted || hasError) {
    return (
      <Box
        flex={1}
        display="flex"
        flexDirection="column"
        justifyContent="space-between"
      >
        <Box my={2}>
          {hasError ? (
            <Alert severity="error">{t("common.errors.request_failed")}</Alert>
          ) : (
            <Alert severity="success">
              {t("common.petition_received_with_email_confirmation")}
            </Alert>
          )}
        </Box>
        <Button onClick={onClickClose} fullWidth={false}>
          {t("common.close")}
        </Button>
      </Box>
    );
  }

  const currentProduct = products.result.data.find(
    (product) => product.code === subscription.active_product_code
  );

  const selectedProduct = products.result.data.find(
    (product) => product.code === value
  );

  const onClickSubmit = async (extraContext) => {
    setNeedsConfirmation(false);
    if (needsInformation) {
      setNeedsInformation(false);
      setIsSelected(true);
      return;
    }
    setIsSubmitting(true);
    try {
      const result = await onSubmit(selectedProduct, extraContext);

      if (result?.error === "needs_confirmation") {
        setNeedsConfirmation(true);
        setHasError(false);
        setIsSubmitted(false);
        setConfirmationContext({ ...result, products: products.result.data });
        return;
      }

      setIsSubmitted(true);
    } catch (e) {
      console.log("e", e);
      setHasError(true);
    } finally {
      setIsSubmitting(false);
    }
  };

  if (isSelected) {
    return (
      <Tiles spacing={2} columns={1}>
        <Alert severity="info">
          {confirmText()}
          <div>
            <Trans i18nKey="common.assistance_email_message">
              <Link
                target="_blank"
                to={"mailto:" + t("common.assistance_email")}
              />
            </Trans>
          </div>
        </Alert>
        <Button
          onClick={() => {
            setIsSelected(false);
            onClickSubmit();
          }}
        >
          {t("common.continue")}
        </Button>
        <Button onClick={onClickClose}>{t("common.cancel")}</Button>
      </Tiles>
    );
  }

  const selectedProductCode = value ? value : currentProduct?.code;

  if (needsConfirmation) {
    return renderConfirmationStep(onClickSubmit, confirmationContext);
  }

  return (
    <>
      <Box display="flex" flexDirection="column" mb={4} alignItems="start">
        <Typography align="center" variant="h5">
          {title}
        </Typography>
      </Box>
      <Box display="flex" flexDirection="column" justifyContent="center" mb={4}>
        <Stack>
          {currentText && (
            <>
              {currentText}
              <strong>{currentProduct.description}</strong>
            </>
          )}
          {descriptionText && <Typography>{descriptionText}</Typography>}
        </Stack>
      </Box>
      <ProductWrapper
        allProducts={products.result.data}
        groupProductsBy={groupProductsBy}
        renderGroupName={renderGroupName}
        renderProductList={(products) => (
          <ProductList
            products={products}
            selectedProductCode={selectedProductCode}
            onProductSelected={(product) => setValue(product.code)}
            renderProductDescription={renderProductDescription}
            priceProductMonthly={priceProductMonthly}
          />
        )}
      />
      <Box p={2} display="flex" flexDirection="column" alignItems="center">
        {selectedProduct && (
          <Typography color="primary" align="center">
            {selectedText}{" "}
            <Hidden mdUp>
              <br />
            </Hidden>
            <strong>{renderProductDescription(selectedProduct)}</strong>
          </Typography>
        )}
        <Box mt={[1, 2]}>
          <Button
            fullWidth={false}
            onClick={() => onClickSubmit()}
            disabled={!selectedProduct}
          >
            {submitText}
          </Button>
        </Box>
      </Box>
    </>
  );
};

export const ProductPickerModal = ({ isOpen, onClose, ...props }) => {
  const [shouldBlockModal, setShouldBlockModal] = useState(false);

  return (
    <Modal
      isOpen={isOpen}
      onClose={shouldBlockModal ? noop : onClose}
      showCloseButton={!shouldBlockModal}
    >
      <ProductPicker
        onLoadingStateChange={(loading) => setShouldBlockModal(loading)}
        onClickClose={onClose}
        {...props}
      />
    </Modal>
  );
};
