import { get } from "axios";

export const getSubscriptionList = () => get("/api/subscriptions/");
export const getSubscriptionDetail = ({ id }) =>
  get(`/api/subscriptions/${id}`);
