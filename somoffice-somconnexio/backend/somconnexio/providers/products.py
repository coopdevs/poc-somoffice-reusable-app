from pyopencell.client import Client
from pyopencell.resources.subscription import Subscription
from somconnexio.presenters.products import Products
from somconnexio.domain.helpers import build_custom_fields_dict
from somconnexio.serializers.products import ProductsSerializer


class ProductsProvider:
    def __init__(self, options):
        self.options = options

    def get_resources(self, subscription_id, product_type):
        if product_type == "additional_data":
            products = self._get_additional_data_products(subscription_id)
        elif product_type == "change_tariff":
            products = self._get_change_tariff_products(subscription_id)

        serializer = ProductsSerializer(products, many=True)
        return serializer.data

    def _get_additional_data_products(self, subscription_id):
        offer_template = self._fetch_offer_template(subscription_id)

        charge_codes = self._get_charge_codes_from(offer_template)

        charges_by_code = {}

        for code in charge_codes:
            charges_by_code[code] = self._fetch_one_shot_charge(code)

        return Products(
            offer_template=offer_template,
            language=self.options["language"],
            subscription_id=subscription_id,
            charges_by_code=charges_by_code,
        ).additional_data_products()

    def _get_change_tariff_products(self, subscription_id):
        offer_template = self._fetch_offer_template(subscription_id)

        return Products(
            offer_template=offer_template,
            language=self.options["language"],
            subscription_id=subscription_id,
        ).change_tariff_products()

    def _get_charge_codes_from(self, offer_template):
        fields_dict = build_custom_fields_dict(
            offer_template["offerTemplate"]["customFields"]["customField"]
        )

        table = fields_dict["CF_OF_SC_REC_TABLE_ADDICIONALS_PRICE"]

        return [code for code in table.keys() if code != "key"]

    def _fetch_one_shot_charge(self, code):
        return Client().get(
            "/catalog/oneShotChargeTemplate", oneShotChargeTemplateCode=code
        )

    def _fetch_offer_template(self, subscription_id):
        subscription = Subscription.get(subscription_id).subscription
        offer_template_code = subscription.offerTemplate

        return Client().get(
            "/catalog/offerTemplate",
            offerTemplateCode=offer_template_code,
            loadOfferServiceTemplate=True,
        )
