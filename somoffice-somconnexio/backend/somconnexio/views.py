from django.views.decorators.http import require_POST, require_GET
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from basicauth.decorators import basic_auth_required
from keycloak import KeycloakAdmin
from pyopencell.resources.customer import Customer
from somoffice.core.email_confirmation import create_email_confirmation_token, send_confirmation_email
from somoffice.models import User
from django.conf import settings
from django.contrib import auth
from somconnexio.serializers.user import UserSerializer
from somconnexio.domain.keycloak import KeycloakRemoteUserBackend
from django_rest_passwordreset.models import ResetPasswordToken
from django_rest_passwordreset.signals import reset_password_token_created
from somconnexio.presenters.user import User as UserPresenter
import json
import secrets
import string


# TODO somoffice.core.register_admin_endpoint(url, view) or something
@csrf_exempt
@basic_auth_required
@require_POST
def import_user(request):
    data = json.loads(request.body)
    customer_code = data["customerCode"]
    customer_email = data["customerEmail"]
    customer_username = data["customerUsername"].lower()

    alphabet = string.ascii_letters + string.digits
    # optionally set this to papapa22 :trollface:
    customer_password = "".join(secrets.choice(alphabet) for i in range(20))

    keycloak_admin = KeycloakAdmin(
        server_url=settings.KEYCLOAK_SERVER_URL + "/auth/",
        username=settings.KEYCLOAK_ADMIN_USER,
        password=settings.KEYCLOAK_ADMIN_PASSWORD,
        realm_name="opencell",
        verify=True,
    )

    # create keycloak user with customerCode attribute
    keycloak_admin.create_user(
        {
            "email": customer_email,
            "username": customer_username,
            "enabled": True,
            "attributes": {"customerCode": customer_code},
        }
    )

    auth_backend = KeycloakRemoteUserBackend()

    # change user password
    auth_backend.change_password(username=customer_username, password=customer_password)

    # authenticate locally to trigger django user creation
    user = auth_backend.authenticate(
        request=None, username=customer_username, password=customer_password
    )

    user.profile.preferredLocale = data.get("customerLocale")
    user.profile.save()

    if data.get("resetPassword"):
        # reset password and send email
        token = ResetPasswordToken.objects.create(
            user=user
        )

        reset_password_token_created.send(sender=None, instance=None, reset_password_token=token)

    return JsonResponse({"msg": "ok"})

@require_GET
@basic_auth_required
@csrf_exempt
def get_user(request):
    try:
        vat = request.GET.get('vat', None).lower()
        keycloak_admin = KeycloakAdmin(
            server_url=settings.KEYCLOAK_SERVER_URL + "/auth/",
            username=settings.KEYCLOAK_ADMIN_USER,
            password=settings.KEYCLOAK_ADMIN_PASSWORD,
            realm_name="opencell",
            verify=True,
        )
        keycloak_user = (
            keycloak_admin.get_users({"username": vat}) or []
        )[0]
        somoffice_user = User.objects.get(username=vat)
        user = UserPresenter(vat,keycloak_user,somoffice_user)
        return JsonResponse(
           UserSerializer(user).data
        )
    except (IndexError, User.DoesNotExist) as e:
        response = JsonResponse({"msg": "User not found"})
        response.status_code = 404
        return response
    except Exception as e:
        print("Unexpected error")
        print(repr(e))
        response = JsonResponse({"msg": "error"})
        response.status_code = 400
        return response


@require_POST
@basic_auth_required
@csrf_exempt
def change_user_email(request):
    try:
        data = json.loads(request.body)
        vat = data["vat"].lower()
        email = data["new_email"]

        user = User.objects.get(username=vat)

        token = create_email_confirmation_token(user, email)
        if token:
            send_confirmation_email(
                user, email, token
            )
        else:
            response = JsonResponse({
                "msg": "Error creating token"
            })
            response.status_code = 400
            return response
    except Exception as e:
        print("Unexpected error")
        print(repr(e))
        response = JsonResponse({
            "msg": "Error sending change email confirmation"
        })
        response.status_code = 400
        return response
    return JsonResponse({"msg": "ok"})
