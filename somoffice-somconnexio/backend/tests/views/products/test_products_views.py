import pytest
from somoffice.models import User, Profile
from rest_framework.test import APIClient
from somoffice.core.secure_resources import build_secure_id_generator


class TestProducts:
    def test_products_without_session(self):
        secure_id_generator = build_secure_id_generator(
            user_id="1859", resource_type="subscription"
        )

        client = APIClient()
        response = client.get(
            f"/api/products/?subscription_id={secure_id_generator('220')}"
        )

        assert response.status_code == 403

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_change_tariff_products(self):
        client, secure_id_generator = self._authenticate()

        response = client.get(
            f"/api/products/?subscription_id={secure_id_generator('220')}&product_type=change_tariff"
        )

        expected_product_codes = [
            "SE_SC_REC_MOBILE_T_0_0",
            "SE_SC_REC_MOBILE_T_0_500",
            "SE_SC_REC_MOBILE_T_0_1024",
            "SE_SC_REC_MOBILE_T_0_2048",
            "SE_SC_REC_MOBILE_T_0_3072",
            "SE_SC_REC_MOBILE_T_0_5120",
            "SE_SC_REC_MOBILE_T_0_10240",
            "SE_SC_REC_MOBILE_T_100_0",
            "SE_SC_REC_MOBILE_T_100_200",
            "SE_SC_REC_MOBILE_T_100_500",
            "SE_SC_REC_MOBILE_T_100_1024",
            "SE_SC_REC_MOBILE_T_100_2048",
            "SE_SC_REC_MOBILE_T_100_3072",
            "SE_SC_REC_MOBILE_T_100_5120",
            "SE_SC_REC_MOBILE_T_100_10240",
            "SE_SC_REC_MOBILE_T_200_0",
            "SE_SC_REC_MOBILE_T_200_200",
            "SE_SC_REC_MOBILE_T_200_500",
            "SE_SC_REC_MOBILE_T_200_1024",
            "SE_SC_REC_MOBILE_T_200_2048",
            "SE_SC_REC_MOBILE_T_200_3072",
            "SE_SC_REC_MOBILE_T_200_5120",
            "SE_SC_REC_MOBILE_T_200_10240",
            "SE_SC_REC_MOBILE_T_300_1024",
            "SE_SC_REC_MOBILE_T_UNL_0",
            "SE_SC_REC_MOBILE_T_UNL_200",
            "SE_SC_REC_MOBILE_T_UNL_500",
            "SE_SC_REC_MOBILE_T_UNL_1024",
            "SE_SC_REC_MOBILE_T_UNL_2048",
            "SE_SC_REC_MOBILE_T_UNL_3072",
            "SE_SC_REC_MOBILE_T_UNL_4096",
            "SE_SC_REC_MOBILE_T_UNL_5120",
            "SE_SC_REC_MOBILE_T_UNL_10240",
            "SE_SC_REC_MOBILE_T_UNL_20480",
            "SE_SC_REC_MOBILE_T_CONSERVA",
            "SE_SC_REC_MOBILE_T_150_0",
            "SE_SC_REC_MOBILE_T_150_500",
            "SE_SC_REC_MOBILE_T_150_1024",
            "SE_SC_REC_MOBILE_T_150_2048",
            "SE_SC_REC_MOBILE_T_150_10240",
            "SE_SC_REC_MOBILE_T_UNL_23552",
            "SE_SC_REC_MOBILE_T_UNL_30720",
            "SE_SC_REC_MOBILE_T_UNL_51200",
        ]

        assert response.status_code == 200
        assert [
            product["code"] for product in response.json()
        ] == expected_product_codes

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_additional_data_products(self):
        client, secure_id_generator = self._authenticate()

        response = client.get(
            f"/api/products/?subscription_id={secure_id_generator('220')}&product_type=additional_data"
        )

        assert response.status_code == 200
        assert len(response.json()) == 4

    def _authenticate(self):
        user = User.objects.create(username="y0136093q")
        Profile.objects.create(
            remoteBackend="opencell", remoteId="1017", preferredLocale="ca", user=user
        )

        secure_id_generator = build_secure_id_generator(
            user_id="1017", resource_type="subscription"
        )

        client = APIClient()

        client.force_authenticate(user=user)

        return (client, secure_id_generator)
