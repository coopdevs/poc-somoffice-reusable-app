## POC separar SomOffice Core en una Django App independiente

En este repositorio podemos encontrar una POC de como podemos empezar a utilizar `somoffice` como un paquete Python.

Encontramos dos directorios:

* `django-somoffice`: Django App paquetizable con el core de SomOfiice
* `somoffice-somconnexio`: Un ejemplo de implementacion de `django-somoffice`.

Estos dos repositorios pueden vivir por separado, ya que solo es necestaio instalar e importar `django-somoffice` en el proyecto `somoffice-somconnexio`.
